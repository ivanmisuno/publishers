
import Combine
import CombineTesting
import Publishers
import XCTest

final class ZipCollectionTests: XCTestCase {

    func testSuccess() {

        let a = PassthroughSubject<Int, Never>()
        let b = PassthroughSubject<Int, Never>()

        let zip = [a, b].zip()
        let subscriber = TestSubscriber<[Int], Never>()

        a.send(1)
        zip.subscribe(subscriber) // Subscription
        b.send(2)
        a.send(3) // [3,2]
        b.send(4)
        a.send(completion: .finished)
        a.send(5)
        b.send(6)
        b.send(completion: .finished) // Completion
        b.send(7)

        XCTAssertEqual(subscriber.events, [
            .subscription,
            .input([3,2]),
            .completion(.finished)
        ])
    }

    func testFailure() {

        let a = PassthroughSubject<Int, TestError>()
        let b = PassthroughSubject<Int, TestError>()
        let zip = [a, b].zip()
        let subscriber = TestSubscriber<[Int], TestError>()

        a.send(1)
        zip.subscribe(subscriber) // subscription
        b.send(2)
        a.send(3) // [3,2]
        b.send(4)
        a.send(completion: .failure("Error")) // completion
        a.send(5)
        b.send(6)
        b.send(completion: .finished)
        b.send(7)

        XCTAssertEqual(subscriber.events, [
            .subscription,
            .input([3,2]),
            .completion(.failure("Error"))
        ])
    }

    func testCancel() {

        let a = PassthroughSubject<Int, Never>()
        let b = PassthroughSubject<Int, Never>()

        let zip = [a, b].zip()
        let subscriber = TestSubscriber<[Int], Never>()

        a.send(1)
        zip.subscribe(subscriber) // Subscription
        b.send(2)
        a.send(3) // [3,2]
        b.send(4)
        subscriber.subscriptions.forEach { $0.cancel() }
        a.send(5)
        b.send(6)
        b.send(completion: .finished)
        b.send(7)

        XCTAssertEqual(subscriber.events, [
            .subscription,
            .input([3,2]),
        ])
    }

  func testReceivedEventsOrder() {
      let a = CurrentValueSubject<Int, Never>(1)
      let b = CurrentValueSubject<Int, Never>(2)

      let zip = [a, b].zip()
      let subscriber = TestSubscriber<[Int], Never>()

      zip.subscribe(subscriber)

      XCTAssertEqual(subscriber.events, [
          .subscription,
          .input([1,2]),
      ])
  }
}
